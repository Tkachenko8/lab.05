# Лабораторна робота №5. Циклічні конструкції

## Мета

Без допомоги зовнішніх бібліотек, реалізувати пограмму з трьома видами циклу(for, while, do while) для виріщення задачі: виявлення чи є число простим

### 1.1 Розробник

Інформація про розробника:
- Ткаченко Дмитро Володимирович; 
- КІТ 121б;

### 1.2 Загальне завдання

- Самостійно написати код для вирішення завдання
- Завантажити код на віддалений репозиторій

### 1.3 Задача
- реалізовати алгоритм виявленнячи є число простим;
- Завантажити код на свій gitlab;

## Пошагові дії, які призвели до необхідного результата
> - Створив директорію laba05
> - Створив директорію src;
> - Створив файл main.c;
> - Оголосивив змінні

	int number = 5;
    int options = 1;

> - Реалізую алгоритм алгоритм вітвлення для вибору одного з циклів;

	if (options == 1){
         cycle_for(number);
       }
       else if (options == 2){
         cycle_while(number);
       }
       else if (options == 3){
         cycle_do_while(number);
       }
       else{
         printf("Некоректно выбран вариант. Попробуйте еще раз"):
       }

> - Реалізація коду з використаннням циклу for;
	
    void cycle_for(int number){
       int flag = 0;
       for(int counter = 2; counter < number/2; counter++){
          if (number%counter == 0){
             printf("Обычное\n");
	         break;
	         flag = 0;
          }
       }
       if (flag == 1){
          printf("Простое\n");
       }
    }

> - Реалізація коду з використаннням циклу while;

    void cycle_while(number){
       int counter = 1;
       int flag = 0;
       while (counter < number/2){
          counter++;
          if (number%counter == 0){
              printf("Обычное\n");
	      break;
	      flag = 0;
          }
       }
       if (flag == 1){
          printf("Простое\n");
       }

> - Реалізація коду з використаннням циклу do while;

    void cycle_do_while(number){
       int counter = 1;
       int flag = 0;
       do{
          counter++;
          if (number%counter == 0){
              printf("Обычное\n");
	      break;
	      flag = 0;
          }
       }while (counter < number/2);
       if (flag == 1){
          printf("Простое\n");
       }
			
## Посилання на створений gitlab аккаунт:
-Gitlab "https://gitlab.com/masterseva2016/lab.s.git"

## Висновок
Була реалізувати пограмму з трьома видами циклу(for, while, do while) для виріщення задачі: виявлення чи є число простим


